import { useState, useRef, useEffect, useCallback, FormEvent, ChangeEvent } from "react";
import { Helmet } from "react-helmet";
import { BrowserRouter, Routes, Route, useParams } from "react-router-dom";

const tld = "localhost:1234";

type Treet = {
    id: number;
    timestamp: string;
    message: string;
};

type User = {
    id?: number;
    username: string;
    password?: string;
};

type UserProps = {
    login: boolean;
};

function Treets() {
    const [treets, setTreets] = useState<Treet[]>([]);
    const [users, setUsers] = useState<User[]>([]);
    const { username } = useParams();
    const ws = useRef<WebSocket>();

    const getUsernameFromId = useCallback((id: number): string => {
        const user = users.find((u: User) => u.id === id);
        return user ? user.username : "";
    }, [users]);

    async function fetchUsers() {
        const response = await fetch(`http://${tld}/json_users`);
        const result = await response.json();
        setUsers((JSON.parse(JSON.stringify(result))));
    }

    useEffect(() => {
        async function fetchTreets() {
            const response = await fetch(`http://${tld}/json_treets/${username ?? ""}`);
            const result = await response.json();
            setTreets((JSON.parse(JSON.stringify(result))));
        }

        fetchTreets();
        fetchUsers();
    }, [username]);

    useEffect(() => {
        ws.current = new WebSocket(`ws://${tld}/websocket`);
        ws.current.onopen = () => console.log("Connection Opened");
        ws.current.onclose = () => console.log("Connection Closed");

        const wsCurrent = ws.current;

        return () => {
            wsCurrent.close();
        };
    }, []);

    useEffect(() => {
        if (!ws.current) return;

        ws.current.onmessage = (message) => {
            const treet = JSON.parse(message.data);
            if (!username || getUsernameFromId(treet.id) === username) {
                setTreets(oldTreets => [...oldTreets, treet]);
            }
            fetchUsers();
        };
    }, [treets, users, username, getUsernameFromId]);

    return (
        <div id="treets">
            <h2>Recent Treets:</h2>
            {treets.map((item: Treet) => (
                <>
                    <div className="box">
                        <h3><a href={`/${getUsernameFromId(item.id)}`}>{getUsernameFromId(item.id)}</a></h3>
                        <h5 style={{ color: "#7d7d7d" }}>{new Date(item.timestamp).toLocaleString()}</h5>
                        <p>{item.message}</p>
                    </div>
                    <br />
                </>
            ))}
        </div>
    );
}

function Users() {
    const [users, setUsers] = useState<User[]>([]);

    async function fetchUsers() {
        const response = await fetch(`http://${tld}/json_users`);
        const result = await response.json();
        setUsers((JSON.parse(JSON.stringify(result))));
    }

    useEffect(() => {
        fetchUsers();
    }, []);

    return (
        <div id="treets">
            <h2>All Users:</h2>
            {users.map((user: User) => (
                <ul>
                    <li><a href={`/${user.username}`}>{user.username}</a></li>
                </ul>
            ))}
        </div>
    );
}

function Navigator() {
    const [message, setMessage] = useState("");
    const [user, setUser] = useState<User>();

    useEffect(() => {
        async function fetchUser() {
            /* const response = await fetch(`http://${tld}/get_user`, { method: "POST", credentials: "include" }); */
            const response = await fetch(`http://${tld}/get_user`, {
                method: "POST",
                headers: { "Content-Type": "application/json" },
                credentials: "include",
            });

            if (response.ok) {
                const result = await response.json();
                const string = JSON.stringify(result);
                const parse = JSON.parse(string);
                setUser(parse);
            }
        }

        fetchUser();
    }, []);

    async function handleLogoutSubmission() {
        try {
            const response = await fetch(`http://${tld}/logout`, {
                method: "POST",
                headers: { "Content-Type": "application/json" },
                credentials: "include",
            });

            setMessage(
                `Logout ${response?.ok
                    ? "succeeded"
                    : "failed"}`
            );
        } catch (error) {
            setMessage("Something went wrong");
        }
    }

    return (
        <div id="nav">
            <a href="/">
                <img src="/images/icon.png" style={{ float: "left", height: "2em", margin: "5px" }} alt="Tritter Logo" />
            </a>
            <h1>
                <a style={{ float: "left", textDecoration: "none", color: "#209cf4" }} href="/">Tritter</a>
            </h1>
            <button style={{ float: "right", margin: "5px" }} onClick={handleLogoutSubmission}>
                <i className="fa fa-fw fa-sign-out"></i> Sign Out
            </button>
            <button style={{ float: "right", margin: "5px" }} onClick={() => (window.location.href = "/login")}>
                <i className="fa fa-fw fa-user"></i> Sign In
            </button>
            <button style={{ float: "right", margin: "5px" }} onClick={() => (window.location.href = "/post")}>
                <i className="fa fa-fw fa-paper-plane"></i> Post Treet
            </button>
            {user && <div id="welcome-message"><p style={{ float: "right", margin: "5px" }}>Welcome, {`${user?.username}`}!</p></div>}
            <div style={{ float: "right", margin: "5px" }} id="message">{message}</div>

            <br style={{ clear: "both" }} />
            <hr />
        </div>);
}

function Menu() {
    return (
        <div id="menu">
            <h3><a href="/"><i className="fa fa-fw fa-home" /> Home</a></h3>
            <h3><a href="/search_treets"><i className="fa fa-fw fa-search" /> Search</a></h3>
            <h3><a href="/users"><i className="fa fa-fw fa-user" /> Users</a></h3>
        </div>
    );
}

function Search() {
    const [treets, setTreets] = useState<Treet[]>([]);
    const [users, setUsers] = useState<User[]>([]);
    const [query, setQuery] = useState("");
    const [results, setResults] = useState<Treet[]>([]);
    const ws = useRef<WebSocket>();

    const getUsernameFromId = useCallback((id: number): string => {
        const user = users.find((u: User) => u.id === id);
        return user ? user.username : "";
    }, [users]);

    function handleQuery(event: ChangeEvent<HTMLInputElement>) {
        event.preventDefault();
        setQuery(event.target.value);
    }

    useEffect(() => {
        async function fetchTreets() {
            const response = await fetch(`http://${tld}/json_treets/`);
            const result = await response.json();
            setTreets((JSON.parse(JSON.stringify(result))));
        }

        async function fetchUsers() {
            const response = await fetch(`http://${tld}/json_users`);
            const result = await response.json();
            setUsers((JSON.parse(JSON.stringify(result))));
        }

        fetchTreets();
        fetchUsers();
    }, []);

    useEffect(() => {
        ws.current = new WebSocket(`ws://${tld}/websocket`);
        ws.current.onopen = () => console.log("Connection Opened");
        ws.current.onclose = () => console.log("Connection Closed");

        const wsCurrent = ws.current;

        return () => {
            wsCurrent.close();
        };
    }, []);

    useEffect(() => {
        if (!ws.current) return;

        ws.current.onmessage = (message) => {
            const treet = JSON.parse(message.data);
            setTreets(oldTreets => [...oldTreets, treet]);
        };
    }, [treets, query]);

    useEffect(() => {
        function contains(treets: Treet[], query: string) {
            const lower = query.toLowerCase();
            return treets.filter((t) => (t.message.toLowerCase().includes(lower) || getUsernameFromId(t.id).toLowerCase().includes(lower)));
        }
        setResults(contains(treets, query));
    }, [treets, query, getUsernameFromId]);

    return (
        <div id="treets">
            <div>
                <h2><label htmlFor="seach-query">Search Query</label></h2>
                <input id="search-query" type="text" required
                    placeholder="Search" onChange={handleQuery} />
            </div>
            <br />
            {results.map((item: Treet) => (
                <>
                    <div className="box">
                        <h3><a href={`/${getUsernameFromId(item.id)}`}>{getUsernameFromId(item.id)}</a></h3>
                        <h5 style={{ color: "#7d7d7d" }}>{new Date(item.timestamp).toLocaleString()}</h5>
                        <p>{item.message}</p>
                    </div>
                    <br />
                </>
            ))}
        </div>
    );
}

function LoginOrRegister({ login }: UserProps) {
    const [username, setUsername] = useState("");
    const [password, setPassword] = useState("");
    const [message, setMessage] = useState("");

    function handleUsername(event: ChangeEvent<HTMLInputElement>) {
        event.preventDefault();
        setUsername(event.target.value);
    }

    function handlePassword(event: ChangeEvent<HTMLInputElement>) {
        event.preventDefault();
        setPassword(event.target.value);
    }

    async function handleSubmission(event: FormEvent<HTMLFormElement>) {
        event.preventDefault();

        const body = {
            username: username,
            password: password
        };

        try {
            const response = await fetch(`http://${tld}/${login ? "login" : "register"}`, {
                method: "POST",
                headers: { "Content-Type": "application/json" },
                body: JSON.stringify(body),
                credentials: "include",
            });

            setMessage(
                response?.ok
                    ? `${login ? "Login" : "Registration"} succeeded`
                    : `${login ? "Login" : "Registration"} failed`
            );
        } catch (error) {
            setMessage("Something went wrong");
        }
    }

    return (
        <>
            <h1>{login ? "Sign In" : "Sign Up"}</h1>
            <form onSubmit={handleSubmission}>
                <div>
                    <h3><label htmlFor="username-input">Username</label></h3>
                    <input id="username-input" type="text" required
                        placeholder="Username" onChange={handleUsername} />
                </div>
                <div>
                    <h3><label htmlFor="password-input">Password</label></h3>
                    <input id="password-input" type="password" required
                        placeholder="Password" onChange={handlePassword} />
                </div>
                <br />
                <button type="submit"><i className="fa fa-fw fa-user"></i> {login ? "Login" : "Register"}</button>
            </form>
            {
                login
                    ? (<p>Don't have an account? Sign up <a href="/register" style={{ textDecoration: "underline", color: "#209cf4" }}>here</a>.</p>)
                    : (<p>Already have an account? Sign in <a href="/login" style={{ textDecoration: "underline", color: "#209cf4" }}>here</a>.</p>)
            }

            <div id="message">{message}</div>
        </>
    );
}

function handleResponse(response: Response, successMessage: string, failureMessage: string): string {
    if (response.ok) {
        return successMessage;
    }
    return failureMessage;
}

function Post() {
    const [contents, setContents] = useState("");
    const [message, setMessage] = useState("");
    const [isSignedIn, setIsSignedIn] = useState<Boolean>();
    const [showSignedIn, setShowSignedIn] = useState<Boolean>();

    useEffect(() => {
        async function fetchUser() {
            /* const response = await fetch(`http://${tld}/get_user`, { method: "POST", credentials: "include" }); */
            const response = await fetch(`http://${tld}/get_user`, {
                method: "POST",
                headers: { "Content-Type": "application/json" },
                credentials: "include",
            });

            if (response.ok) {
                setIsSignedIn(true);
            }
        }
        fetchUser();
    }, []);

    useEffect(() => {
        setTimeout(() => setShowSignedIn(true), 1000);
    }, []);

    function handleMessage(event: ChangeEvent<HTMLTextAreaElement>) {
        event.preventDefault();
        setContents(event.target.value);
    }

    async function handleTreetSubmission(event: FormEvent<HTMLFormElement>) {
        event.preventDefault(); // This prevents the form from being submitted automatically

        // Create the object to be sent in the body of the request
        const body = {
            // username: username.value,
            message: contents
        };

        // Send a POST request to http://${tld}/make_treet with the data in the body object as the request body.
        let response = await fetch(`http://${tld}/make_treet`, {
            method: "POST",
            headers: { "Content-Type": "application/json" }, // Tells the server what type of data to expect (JSON)
            body: JSON.stringify(body), // Turn the object into a string representing JSON
            credentials: "include"
        });

        setMessage(handleResponse(response, "Treet Created", "Unable to create treet"));
    }

    return (
        <>
            <form id="post-form" onSubmit={handleTreetSubmission}>
                <div>
                    <div>
                        <label htmlFor="message-input">
                            <h2>Message</h2>
                        </label>
                    </div>
                    <textarea id="message-input" rows={5} cols={40}
                        placeholder="Treet message" required onChange={handleMessage}></textarea>
                </div>

                <br />

                <button type="submit"><i className="fa fa-fw fa-paper-plane"></i>
                    Submit</button>
            </form>
            <br />
            {showSignedIn && !isSignedIn && <div id="signin-message">"You need to sign in before posting a Treet"</div>}

            <div id="message">{message}</div>
        </>

    );
}

function App() {
    return (
        <BrowserRouter>
            <Helmet>
                <title>Tritter</title>
                <link rel="icon" type="image/x-icon" href="/images/favicon.ico" />
                <link
                    rel="stylesheet"
                    href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css"
                />
                <meta name="viewport" content="width=device-width,initial-scale=1" />
            </Helmet>

            <Navigator />

            <Routes>
                <Route path="/login" element={
                    <LoginOrRegister login={true} />
                } />
                <Route path="/register" element={
                    <LoginOrRegister login={false} />
                } />
                <Route path="/post" element={
                    <Post />
                } />
                <Route path="/search_treets" element={
                    <div id="main">
                        <Menu />
                        <Search />
                    </div>
                } />
                <Route path="/users" element={
                    <div id="main">
                        <Menu />
                        <Users />
                    </div>
                } />
                <Route path=":username?" element={
                    <div id="main">
                        <Menu />
                        <Treets />
                    </div>
                } />
            </Routes>
        </BrowserRouter>
    );
}
export default App;
