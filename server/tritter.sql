DROP TABLE IF EXISTS Treets;
DROP TABLE IF EXISTS Users;
CREATE TABLE Treets (id INTEGER, timestamp TEXT, message TEXT);
INSERT INTO Treets VALUES (1, '2022-03-10T02:00:00Z', 'I am totally not a robot');
INSERT INTO Treets VALUES (1, '2022-03-17T23:17:10Z', 'I can pass the turing test perfectly');
INSERT INTO Treets VALUES (1, '2022-03-19T08:54:17Z', 'beep beep beep');
INSERT INTO Treets VALUES (2, '2022-02-17T15:43:08Z', 'C is for cookie!');
INSERT INTO Treets VALUES (2, '2022-03-19T08:54:17Z', 'Cookies!!!');
INSERT INTO Treets VALUES (2, '2022-04-01T20:37:30Z', 'Om nom nom nom!!!');
INSERT INTO Treets VALUES (3, '2022-03-24T07:12:54Z', 'We''re no stranger''s to love');
INSERT INTO Treets VALUES (3, '2022-03-24T07:13:54Z', 'You know the rules, and so do I');
INSERT INTO Treets VALUES (3, '2022-03-24T07:14:54Z', 'Full commitment is what I''m thinking of');
INSERT INTO Treets VALUES (3, '2022-03-24T07:15:54Z', 'You wouldn''t get this far with any other guy');

CREATE TABLE Users (id INTEGER PRIMARY KEY AUTOINCREMENT, username TEXT, password TEXT);
INSERT INTO Users VALUES (1, 'MrRoboto', '$2b$10$FITjCrhjc571/sHBg1oyVOHNGJ4Z2XVHsQWT2zqivrB.XPmMh/Kta'); -- kiwhreg91382
INSERT INTO Users VALUES (2, 'Cookie_Monster', '$2b$10$hI4cmGOSNquBOH1AALQ3XOXwcBrH4Z5eea59khFh28f6bWQ8slyE6'); -- 123$12333
INSERT INTO Users VALUES (3, 'RickAstley', '$2b$10$vSKv9RfnzKLASkXY7eu1IOFSYXnM4h.NlxtUMMI3bPzrPG7.xe3ia'); -- smartoo122k