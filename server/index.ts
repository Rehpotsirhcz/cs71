// Import the express variable from the expression package, which is a dependency specified in package.json
import express, { Express, Request, Response } from "express";
import http from "http";
import cors from "cors";
import * as path from "path";
import session from "express-session";

// Import functions from database-adapter.ts
import { getAllTreets, getUser, getAllUsers, getTreetsByUsername, handleLoginRequest, handleLogoutRequest, handleRegistrationRequest, makeTreet } from "./database-adapter";
//
// Import functions from websocket-server.ts
import { attachWebSocketServer } from "./websocket-server";

// Create an express server app
const app = express();

app.use(express.json()); // Only handle requests that have JSON body and "application/json" Content-Type header
app.use(cors({ origin: true, credentials: true })); // Enable Cross Origin Requests
app.use(session({
    secret: "<pPn5)0_X=HTrR09do)b*O]Y#%$l+Wi8Rl90`c&y7XRR8;N+",
    resave: false,
    saveUninitialized: false
}));

app.post("/make_treet", async (request: Request, response: Response) => {
    if (await makeTreet(request)) {
        return response.status(200).json(request.body);
    } else {
        return response.status(401).json(request.body);
    }
});

app.post("/register", handleRegistrationRequest);
app.post("/login", handleLoginRequest);
app.post("/logout", handleLogoutRequest);

app.post("/get_user", async (request: Request, response: Response) => {
    const user = await getUser(request);
    if (user) {
        return response.status(200).json(user);
    } else {
        return response.status(401);
    }
});

// Define a lambda to handle a GET request to route "/json_treets" on the server
// NOTE The lambda takes in 2 inputs. They are the request and the response objects
app.get("/json_treets", async (req, res) => {
    console.log(`Received request to ${req.originalUrl}`);
    res.send(await getAllTreets());
});

// Define a lambda to handle a GET request to route "/json_treets/:username", where :username could be any string
app.get("/json_treets/:username", async (req, res) => {
    console.log(`Received request to ${req.originalUrl}`);
    const name = req.params.username;
    res.send(await getTreetsByUsername(name));
});

// Define a lambda to handle a GET request to route "/json_users" on the server
app.get("/json_users", async (req, res) => {
    console.log(`Received request to ${req.originalUrl}`);
    res.send(await getAllUsers());
});

const port = 1234;

// Starts the web server on the specified port
// Go to: http://${tld}/ or http://${tld}/json_treets/username
const httpServer = http.createServer(app);
attachWebSocketServer("/websocket", httpServer);
httpServer.listen(port, () => console.log(`Started web server on port ${port}`));
