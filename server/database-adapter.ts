import * as bcrypt from "bcrypt";
import { Model, InferAttributes, InferCreationAttributes, Sequelize, DataTypes } from "sequelize";
import { Request, Response } from "express";
import WebSocket from "ws";

const tld = "localhost:1234";

const sequelize = new Sequelize({
    dialect: "sqlite",
    storage: "server/tritter.db"
});

type Treets = {
    id?: number;
    timestamp: string;
    message: string;
};

type Users = {
    id?: number;
    username: string;
    password: string;
};

export interface MTreet extends Model<InferAttributes<MTreet>, InferCreationAttributes<MTreet>>, Treets {
};

export interface MUser extends Model<InferAttributes<MUser>, InferCreationAttributes<MUser>>, Users {
};

const TreetModel = sequelize.define<MTreet>("Treets", {
    id: { type: DataTypes.INTEGER, primaryKey: true, autoIncrement: false },
    timestamp: DataTypes.STRING,
    message: DataTypes.STRING
}, {
    freezeTableName: true,
    timestamps: false,
});

const UserModel = sequelize.define<MUser>("Users", {
    username: DataTypes.STRING,
    password: DataTypes.STRING
}, {
    freezeTableName: true,
    timestamps: false,
});

export async function getAllTreets(): Promise<MTreet[]> {
    return await TreetModel.findAll({ raw: true, order: ["timestamp"] });
}

export async function getAllUsers(): Promise<MUser[]> {
    return await UserModel.findAll({ raw: true, order: ["id"] });
}

export async function getTreetsByUsername(username: string): Promise<MTreet[]> {
    const user = await getUserByUsername(username);
    const id = user?.id;
    try {
        return await TreetModel.findAll({
            where: {
                id
            },
            raw: true,
            order: ["id"]
        }
        );
    } catch {
        return [];
    }
}

export async function getUserByUsername(username: string): Promise<MUser | null> {
    await UserModel.sync();
    // findOne will either return the found data, or null if no data was found
    const foundUser = await UserModel.findOne({
        where: {
            username
        },
        raw: true,
    });
    return foundUser; // get only the data we want (username and password)
}

export async function makeTreet(request: Request): Promise<boolean> {
    return TreetModel.sync().then(async () => {
        const user = request.session.user;
        const message = request.body.message;
        const id = user?.id;
        const timestamp = new Date().toISOString();
        if (id) {
            TreetModel.create({ id, timestamp, message });
            const ws = new WebSocket(`ws://${tld}/websocket`);
            ws.onopen = (event) => {
                const result = { id: id, timestamp: timestamp, message: message };
                ws.send(JSON.stringify(result));
            };
            return true;
        } else {
            return false;
        }
    });
}

async function makeUser(user: MUser): Promise<MUser> {
    await UserModel.sync();
    const createdUser = await UserModel.create({
        username: user.username,
        password: bcrypt.hashSync(user.password, 10)
    });
    return createdUser;
}

async function registerUser(user: MUser): Promise<MUser> {
    try {
        const foundUser = await getUserByUsername(user.username);
        // if found user is not null, we know someone already has the given username
        if (foundUser) {
            return Promise.reject("A user with the given username already exists");
        }
        return await makeUser(user);
    } catch (error) {
        return Promise.reject(error);
    }
}

export async function handleRegistrationRequest(request: Request, response: Response) {
    const data = request.body;
    try {
        // if the registration is successful, then respond 200 (OK) with a body that has the username
        const registeredUser = await registerUser(data as MUser);
        return response.status(200).json({
            username: registeredUser.username,
        });
    } catch (error) {
        // if a reject occurs at any point from the call chain of "registerUser" then something went wrong
        return response.status(400).json({
            message: error,
        });
    }
}

export async function handleLoginRequest(request: Request, response: Response) {
    try {
        const responseMessage = await login(request);
        return response.status(200).json({ message: responseMessage });
    } catch (error) {
        return response.status(401).json({ error: error });
    }
}

export async function login(request: Request): Promise<string> {
    try {
        const user: MUser = request.body;
        const foundUser = await getUserByUsername(user.username);
        if (foundUser) {
            if (bcrypt.compareSync(user.password, foundUser.password)) {
                request.session.user = foundUser;
                return Promise.resolve("Login succeeded");
            }
        }
        return Promise.reject("Login failed");
    } catch (error) {
        return Promise.reject(error);
    }
}

export async function handleLogoutRequest(request: Request, response: Response) {
    try {
        const responseMessage = await logout(request);
        return response.status(200).json({ message: responseMessage });
    } catch (error) {
        return response.status(401).json({ error: error });
    }
}

export async function logout(request: Request): Promise<string> {
    try {
        if (request.session.user) {
            request.session.user = undefined;
            return Promise.resolve("Logout succeeded");
        }
        return Promise.reject("Logout failed");
    } catch (error) {
        return Promise.reject(error);
    }
}

export async function getUser(request: Request): Promise<MUser | undefined> {
    if (request.session.user) {
        return Promise.resolve(request.session.user);
    }
    return Promise.resolve(undefined);
}

declare module "express-session" {
    interface SessionData {
        user: MUser;
    }
}
