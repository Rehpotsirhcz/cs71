import WebSocket from "ws";
import * as http from "http";

// attach a websocket server to an existing http server
export function attachWebSocketServer(websocketRoute: string, httpServer: http.Server) {
    const websocketServer = new WebSocket.Server({
        path: websocketRoute,
        server: httpServer
    });

    // array of active websocket connections
    let sockets: WebSocket.WebSocket[] = [];

    // when a websocket connects to the server....
    websocketServer.on("connection", function (socket) {
        // add active websocket into the sockets array
        sockets.push(socket);

        // broadcast to everyone that a new socket is connected
        // broadcastConnectionCount();

        // When receiving a message, forward that message to every connected socket.
        socket.on("message", function (msg) {
            const message = msg.toString();
            console.log("Received message: " + message);
            sockets.forEach(s => s.send(message));
        });

        // When a socket closes....
        socket.on("close", function () {
            // remove connection from the sockets array
            sockets = sockets.filter(s => s !== socket);
            // broadcastConnectionCount();
        });
    });

    // function broadcastConnectionCount() {
    //     sockets.forEach(s => s.send("Active connections: " + sockets.length));
    // }

    return websocketServer;
}
